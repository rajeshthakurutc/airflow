#!/usr/bin/env python
"""sparky_commands.py: Set of commands to test and connect Airflow to Predikto Sparky."""

__author__ = "Gula Nurmatova"

from putils.Interfaces import ElasticSearchInterface
from sparky.dag import DAG
from . import mock_cdb
from nike.services.meta_data import MetaData
from nike.services.meta_anomaly_score import MetaAnomalyScore
from nike.services.cache_invalidator import CacheInvalidator
from nike.services.deleter import Deleter
from nike.services.meta_data_anomalies import MetaDataAnomalies
from nike.services.models_metrics import ModelsMetrics
from nike.services.mover import Mover
from nike.services.notifier import Notifier
from nike.services.qa_report import QAReport
from nike.services.querier import Querier
from nike.services.reindexer import ReIndexer
from nike.services.snapshot import Snapshot
from nike.services.test_service import TestService
from nike.services.tiers import Tiers
from nike.services.validator import Validator
from larry_services.services import ESClassification
from larry_services.services import Classification
from larry_services.services import FeatureSelection
from larry_services.services import ModelScoring
from larry_services.services import ThresholdOptimization
import os

class DSCommand(object):
    host_name = os.environ["ES_HOST"]
    host_port = os.environ["ES_PORT"]
    esi = None
    models_index = ''
    class_map={
        "METADATA":MetaData,
        "METAANOMALYSCORE": MetaAnomalyScore,
        "CACHEINVALIDATOR":CacheInvalidator,
        "DELETER":Deleter,
        "METADATAANOMALIES":MetaDataAnomalies,
        "MODELMETRICS":ModelsMetrics,
        "MOVER":Mover,
        "NOTIFIER":Notifier,
        "QANOTIFIER":QAReport,
        "QUERIER":Querier,
        "REINDEXER":ReIndexer,
        "SNAPSHOT":Snapshot,
        "TESTSERVICE":TestService,
        "TIERS":Tiers,
        "VALIDATOR":Validator,
        "ESCLASSIFICATION":ESClassification,
        "CLASSIFICATION":Classification,
        "FEACHURESELECTION":FeatureSelection,
        "MODELSCORING":ModelScoring,
        "THRESHOLDOPTIMIZATION":ThresholdOptimization
    }

    cdb = None
    try:
        pass
        #cdb = CentralDBInterface(host="etl-test.ckyw1pwlibsb.us-east-1.rds.amazonaws.com", username="predikto", password="big$exit!1")
    except:
        cdb = None

    def __init__(self):
        self.esi = ElasticSearchInterface(host_name=self.host_name, host_port=self.host_port)

    def serviceFactory(self, ds, kwargs):
        print("DSCommand received the service call:") #this will go to Airflow logs
        print(kwargs)
        service_type = kwargs['params']['service_type']
        if(service_type == "SPARKY"):
            self.rundag(ds=ds, kwargs=kwargs)
        else:
            srv = self.class_map[service_type.upper()](central_db=self.cdb, kwargs=kwargs['params'])
            srv.run()


    #sparky service
    def rundag(self, ds, kwargs):
        config = {
        'runner': {
            'project_id': 'test_local_spark',
            'log_processed': 'False',
            'data_source': {
                'location': 'local',
            },
        },
        'dag': {
                "BATCH_FEATURE_SCORE": {
                    "after": "None",
                    "args": {
                        "asset_level_key": "device_id",
                        "batch_size": 200,
                        "db_schema": "public",
                        "db_table_name": "jsonb_test",
                        "drop_columns": [
                            "date_epoch",
                            "device_id",
                            "set_indicator"
                        ],
                        "es_agg_table": "test_agg_table",
                        "es_host_name": "localhost",
                        "es_host_port": "9200",
                        "features_index": "test_features_v1",
                        "features_mapping": "data",
                        "jsonb_column": "feature_info",
                        "max_epoch": None,
                        "min_epoch": None,
                        "models_index": "test_models_index",
                        "models_mapping": "data",
                        "n_score": 1000,
                        "only_active": True,
                        "rating_type": "elo",
                        "scoreboard_index": "jmi_locomotive_scoreboard",
                        "scorewise":"pairwise",
                        "scoreboard_mapping": "data",
                        "scorer": "ExtraTreesClassifierScorer",
                        "selected_features": [
                        ],
                        "tier_list": [
                            "tier_1"
                        ]
                    },
                    "type": "batch_feature_score"
                }
            },
        }
        runner = DAG(
            config,
            ENV='MOCK',
            dns='mock',
        )
        runner.run(debug=True)
