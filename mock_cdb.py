class MockCDB():
    def __init__(self, models=None, proc=None, preds=None, scoreboard=None):
        self.models = models
        self.proc = proc
        self.preds = preds
        self.scoreboard = scoreboard

    def get_elastic_search_index(self, customer_id, data_type):
        if data_type == 'MODELS':
            return self.models
        elif data_type == 'EVENTS':
            return self.proc
        elif data_type == 'SCOREBOARD':
            return self.scoreboard
        elif data_type == 'PRD_OUT':
            return self.preds
        else:
            raise ValueError()

    def start_task(self, *args, **kwargs):
        pass

    def finish_task(self, *args, **kwargs):
        pass

    @property
    def driver(self):
        class MockDriver():
            def __init__(self, models=None, proc=None, preds=None, scoreboard=None):
                self.models = models
                self.proc = proc
                self.preds = preds
                self.scoreboard = scoreboard

            def execute(self, *args, **kwargs):
                return ['localhost']

            def get_elastic_search_index(self, customer_id, data_type):
                if data_type == 'MODELS':
                    return self.models
                elif data_type == 'EVENTS':
                    return self.proc
                elif data_type == 'SCOREBOARD':
                    return self.scoreboard
                elif data_type == 'PRD_OUT':
                    return self.preds
                else:
                    raise ValueError()

        return MockDriver(self.models, self.proc, self.preds, self.scoreboard)

    def get_key_value(self, foo):
        return 'localhost'
