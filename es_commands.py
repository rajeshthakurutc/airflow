#!/usr/bin/env python
"""es_commands.py: Set of commands to test and connect Airflow to Predikto Elastic Search Data Store."""

__author__ = "Gula Nurmatova"

import time
import random
import os
from putils.Interfaces.ElasticSearchInterface import ElasticSearchInterface
import os

class ElasticCommand(object):
    host_name = os.environ["ES_HOST"]
    host_port = os.environ["ES_PORT"]
    esi = None
    index = ''

    def __init__(self):
        self.esi = ElasticSearchInterface(host_name=self.host_name, host_port=self.host_port)

    def serviceFactory(self, ds, kwargs):
        print("ElasticCommand received the service call:") #this will go to Airflow logs
        print(kwargs)
        service_type = kwargs['params']['service_type']
        if(service_type == "ELASTIC_DATA_INSERT"):
            self.insertData(ds=ds, kwargs=kwargs)
        if(service_type == "ELASTIC_INDEX_DELETE"):
            self.deleteIndex(ds=ds, kwargs=kwargs)
        if(service_type == "LOAD_NIKE_METADATA"):
            self.loadNikeMetadata(ds=ds, kwargs=kwargs)
        if (service_type == "LOAD_NIKE_META_ANOMALY_SCORE"):
            self.loadMetaAnomalyScore(ds=ds, kwargs=kwargs)
        if(service_type == "LOAD_LARRY_THRESHOLDOPTIMIZATION"):
            self.loadLarryThresholdOptimization(ds=ds, kwargs=kwargs)

    def insertData(self, ds, kwargs):
        self.models_index = kwargs['params']['index_name']
        data_to_start = kwargs['params']['data_to_insert']
        self.esi.driver.insert_lines(data=data_to_start, index=self.models_index, data_type='data')

    def deleteIndex(self, ds, kwargs):
        self.models_index = kwargs['params']['index_name']
        #TODO: implement Delete, it looks like 2 interfaces for ES are used - one from the elasticsearch and one from putils
        #Need to understand why and what can be done to remove ambiguity
        self.esi.indices.delete(self.models_index) #<-- this does not work

    def loadLarryThresholdOptimization(self, ds, kwargs):
        self.models_index = "test_model_index"
        self.processed_data_index = "test_processed_data_index"
        self.prediction_output_index = "test_prediction_data_index"
        self.build_models_index()
        self.build_processed_data_index()
        self.build_prediction_data_index()
        self.esi.driver.client.indices.refresh(self.models_index)
        self.esi.driver.client.indices.refresh(self.processed_data_index)
        self.esi.driver.client.indices.refresh(self.prediction_output_index)

    def build_models_index(self):
        models_doc = [
            {
                "tier_name": "tier_1",
                "pre_col": {
                    "f_event": ""
                },
                "threshold_windows": {
                    "medium_risk": {
                        "upper_bound": 0.75,
                        "lower_bound": 0.5
                    },
                    "no_risk": {
                        "upper_bound": 0.0,
                        "lower_bound": 0.25
                    },
                    "critical": {
                        "upper_bound": 1,
                        "lower_bound": 0.75
                    },
                    "low_risk": {
                        "upper_bound": 0.25,
                        "lower_bound": 0.5
                    }
                },
                "asset_level_id": "device_id",
                "pipeline_args": {
                    "pipeline": [
                        {"name": "imputer", "type": "zeroimputer", "args": {},
                         "wrappers": [{"type": "transform", "args": {}}]},
                        {"name": "binary", "type": "hashing-encoder", "args": {"cols": None},
                         "wrappers": [{"type": "transform", "args": {}}]},
                        {"name": "random forest", "type": "randomforest", "args": {},
                         "wrappers": [{"type": "predict_proba", "args": {}}]}
                    ]
                },
                "interpretation_args": {},
                "event_info": {"query_terms": [{"param_name": "f_event", "term": "1"}],
                               "pretty_name": "f_event"},
                'agg_index_spec':  {"spec": { "agg": "daily", "offset": { "hours": "0", "minutes": "0", "days": "0" } }, "epoch_name": "date_epoch", "param_name": "time_stamp", "metric_type": "timestamp" }


            }
        ]
        self.esi.driver.insert_lines(models_doc, self.models_index, 'data')

    def build_processed_data_index(self):
        docs = [
            {"f_event": 1, "date_epoch": 1, 'device_id': 'device_id1', 'time_stamp': 86400001},
            {"f_event": 1, "date_epoch": 1, 'device_id': 'device_id2', 'time_stamp': 86400001},
            {"f_event": 1, "date_epoch": 1, 'device_id': 'device_id3', 'time_stamp': 86400001},
        ]
        self.esi.driver.insert_lines(docs, self.processed_data_index, 'data')

    def build_prediction_data_index(self):
        docs = [
            {"date_epoch": 1, "prediction": {"tier_1": {"final_prediction_pr": .01}}, "description": "tier_1", "device_id": "device_id1"},
            {"date_epoch": 2, "prediction": {"tier_1": {"final_prediction_pr": .70}}, "description": "tier_1", "device_id": "device_id1"},
            {"date_epoch": 3, "prediction": {"tier_1": {"final_prediction_pr": .90}}, "description": "tier_1", "device_id": "device_id1"},
            {"date_epoch": 1, "prediction": {"tier_1": {"final_prediction_pr": .01}}, "description": "tier_1",
             "device_id": "device_id2"},
            {"date_epoch": 2, "prediction": {"tier_1": {"final_prediction_pr": .70}}, "description": "tier_1",
             "device_id": "device_id2"},
            {"date_epoch": 3, "prediction": {"tier_1": {"final_prediction_pr": .90}}, "description": "tier_1",
             "device_id": "device_id2"},
            {"date_epoch": 1, "prediction": {"tier_1": {"final_prediction_pr": .01}}, "description": "tier_1",
             "device_id": "device_id3"},
            {"date_epoch": 2, "prediction": {"tier_1": {"final_prediction_pr": .70}}, "description": "tier_1",
             "device_id": "device_id3"},
            {"date_epoch": 3, "prediction": {"tier_1": {"final_prediction_pr": .90}}, "description": "tier_1",
             "device_id": "device_id3"},
        ]
        self.esi.driver.insert_lines(docs, self.prediction_output_index, 'data')


    def loadNikeMetadata(self, ds, kwargs):
        self.index = kwargs['params']['index_name']
        st = int(time.time() * 1000)
        mapping_name = 'data'

        temp_index_name = self.index
        self.esi.create_index(temp_index_name)
        self.esi.create_mapping(temp_index_name, mapping_name)
        time.sleep(2)
        data = [
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st + int(0.5 * 24 * 3600 * 1000),
             'bool': False, 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st + int(1.5 * 24 * 3600 * 1000),
             'bool': False, 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st + int(2.5 * 24 * 3600 * 1000),
             'bool': False, 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st + int(3.5 * 24 * 3600 * 1000),
             'bool': True, 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st + int(4.5 * 24 * 3600 * 1000),
             'bool': True, 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st + int(5.5 * 24 * 3600 * 1000),
             'bool': True, 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st + int(6.5 * 24 * 3600 * 1000),
             'bool': True, 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st + int(7.5 * 24 * 3600 * 1000),
             'bool': True, 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st + int(0.5 * 24 * 3600 * 1000),
             'bool': False, 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st + int(1.5 * 24 * 3600 * 1000),
             'bool': False, 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st + int(2.5 * 24 * 3600 * 1000),
             'bool': False, 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st + int(3.5 * 24 * 3600 * 1000),
             'bool': True, 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st + int(4.5 * 24 * 3600 * 1000),
             'bool': True, 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st + int(5.5 * 24 * 3600 * 1000),
             'bool': True, 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st + int(6.5 * 24 * 3600 * 1000),
             'bool': True, 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st + int(7.5 * 24 * 3600 * 1000),
             'bool': True, 'float': 100 * random.random()},
        ]
        data += [
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'bool': False,
             'time_stamp': st + int(0.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'bool': False,
             'time_stamp': st + int(1.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'bool': False,
             'time_stamp': st + int(2.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'bool': True,
             'time_stamp': st + int(3.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'bool': True,
             'time_stamp': st + int(4.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'bool': True,
             'time_stamp': st + int(5.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'bool': True,
             'time_stamp': st + int(6.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'bool': True,
             'time_stamp': st + int(7.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'A', 'bool': False,
             'time_stamp': st + int(0.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'bool': False,
             'time_stamp': st + int(1.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'A', 'bool': False,
             'time_stamp': st + int(2.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'bool': True,
             'time_stamp': st + int(3.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'A', 'bool': True,
             'time_stamp': st + int(4.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'bool': True,
             'time_stamp': st + int(5.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'A', 'bool': True,
             'time_stamp': st + int(6.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'bool': True,
             'time_stamp': st + int(7.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
        ]
        data += [
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(0.5 * 24 * 3600 * 1000),
             'bool': False, 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(1.5 * 24 * 3600 * 1000),
             'bool': False, 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(2.5 * 24 * 3600 * 1000),
             'bool': False, 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(3.5 * 24 * 3600 * 1000),
             'bool': False, 'float': 100 * random.random()},

            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'bool': False,
             'time_stamp': st - int(0.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'bool': False,
             'time_stamp': st - int(1.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'bool': False,
             'time_stamp': st - int(2.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'bool': False,
             'time_stamp': st - int(3.5 * 24 * 3600 * 1000) - int(30 * 24 * 3600 * 1000), 'float': 100 * random.random()},
        ]
        data = [
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(0.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(1.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(2.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(3.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(4.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(5.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(6.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(7.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st - int(0.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st - int(1.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st - int(2.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st - int(3.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st - int(4.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st - int(5.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st - int(6.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st - int(7.5 * 24 * 3600 * 1000),
             'float': 100*random.random()},
        ]
        self.esi.driver.insert_lines(data, temp_index_name, mapping_name)

        data = [

            {'event': 1, 'event_type': 'electrical', 'device_id': 'AAA', 'time_stamp': st - int(0.5 * 24 * 3600 * 1000),
             'var1': 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'BBB', 'time_stamp': st - int(1.5 * 24 * 3600 * 1000),
             'var1': 100 * random.random(), 'var2': 1500 + 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'CCC', 'time_stamp': st - int(2.5 * 24 * 3600 * 1000),
             'var1': 100 * random.random(), 'var2': 1500 + 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'DDD', 'time_stamp': st - int(3.5 * 24 * 3600 * 1000),
             'var1': 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'EEE', 'time_stamp': st - int(4.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'FFF', 'time_stamp': st - int(5.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(6.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'B', 'time_stamp': st - int(7.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(8.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'B', 'time_stamp': st - int(9.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 1, 'event_type': 'electrical', 'device_id': 'A', 'time_stamp': st - int(10.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 0, 'event_type': 'electrical', 'device_id': 'B', 'time_stamp': st - int(11.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},

            {'event': 0, 'event_type': 'mechanical', 'device_id': 'GGG', 'time_stamp': st - int(0.5 * 24 * 3600 * 1000),
             'var1': 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'HHH', 'time_stamp': st - int(1.5 * 24 * 3600 * 1000),
             'var1': 100 * random.random(), 'var2': 1500 + 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'III', 'time_stamp': st - int(2.5 * 24 * 3600 * 1000),
             'var1': 100 * random.random(), 'var2': 1500 + 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'JJJ', 'time_stamp': st - int(3.5 * 24 * 3600 * 1000),
             'var1': 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'KKK', 'time_stamp': st - int(4.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'III', 'time_stamp': st - int(5.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'B', 'time_stamp': st - int(6.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st - int(7.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'B', 'time_stamp': st - int(8.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st - int(9.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 0, 'event_type': 'mechanical', 'device_id': 'B', 'time_stamp': st - int(10.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()},
            {'event': 1, 'event_type': 'mechanical', 'device_id': 'A', 'time_stamp': st - int(11.5 * 24 * 3600 * 1000),
             'var1': 1500 + 100 * random.random(), 'var2': 100 * random.random()}

        ]

        try:
            processed_data_index = kwargs['params']['processed_data_index']
        except:
            processed_data_index = 'test_metadata_processed_data'
        self.esi.driver.insert_lines(data, processed_data_index, mapping_name)

        try:
            metadata_anomalies_index = kwargs['params']['metadata_anomalies_index']
            metadata_anomalies_mapping = kwargs['params']['mapping']
        except:
            metadata_anomalies_index = "test_metadata_processed_data"
            metadata_anomalies_mapping = "data"
        self.esi.create_index(metadata_anomalies_index)
        self.esi.create_mapping(metadata_anomalies_index, metadata_anomalies_mapping)

    def loadMetaAnomalyScore(self, ds, kwargs):
        ts_begin = 1483228800000  # 2017-01-01
        ts_end = 1491868800000  # 2017-04-07
        ts_day = 24 * 60 * 60 * 1000  # 1 day in milliseconds.
        ts_interval_end_1 = 1485907200000  # 2017-02-01
        ts_interval_end_2 = 1487116800000  # 2017-02-15

        processed_data_index_name1 = 'test_processed_data'
        self.esi.create_index(processed_data_index_name1)
        self.esi.create_mapping(self.processed_data_index_name1, 'meta_data')
        self.esi.create_mapping(self.processed_data_index_name1, 'meta_data_zscore')
        self.esi.client.indices.refresh(self.processed_data_index_name1)
        time.sleep(2)
        self.populate_meta_data()
        self.esi.client.indices.refresh(self.processed_data_index_name1)
        time.sleep(2)
