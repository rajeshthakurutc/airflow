from setuptools import find_packages
from setuptools import setup
from version import __version__ as VERSION

install_requires = ["apache-airflow"]
tests_require = []
dev_requires = tests_require + ["black", "ipdb", "flake8", "pre-commit"]

setup(
    author="Gula Nurmatova",
    author_email="gula.nurmatova@utc.com",
    description="commands to be used to trigger as airflow tasks for Predikto MAX",
    extras_require={"test": tests_require, "dev": dev_requires},
    install_requires=install_requires,
    name="predikto-airflow-dag",
    packages=find_packages(),
    url="https://www.python.org/sigs/distutils-sig/",
    version=VERSION
)
